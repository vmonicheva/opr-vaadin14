package ru.my.opr_vaadin;

import org.hibernate.cfg.AvailableSettings;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(exclude = ErrorMvcAutoConfiguration.class)
@EnableJpaRepositories(basePackages = "ru.my.opr_vaadin.repository")
@EnableScheduling
public class Application {
	static {
		System.setProperty(AvailableSettings.HBM2DLL_CREATE_SCHEMAS, "true");
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
