package ru.my.opr_vaadin.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface VisualEntity {
	@JsonProperty("_string")
	String asString();

	long getId();
}
