package ru.my.opr_vaadin.repository;

import org.springframework.data.repository.CrudRepository;
import ru.my.opr_vaadin.dto.Alert;
import ru.my.opr_vaadin.dto.Transaction;

import java.util.List;

public interface AlertRepository extends CrudRepository<Alert, Long> {
	@Override
	List<Alert> findAll();

	List<Alert> findAllByTransactionIn(List<Transaction> transactions);
}
