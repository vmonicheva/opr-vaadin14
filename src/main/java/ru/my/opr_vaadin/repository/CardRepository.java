package ru.my.opr_vaadin.repository;

import org.springframework.data.repository.CrudRepository;
import ru.my.opr_vaadin.dto.Card;
import ru.my.opr_vaadin.dto.Client;

import java.util.List;

public interface CardRepository extends CrudRepository<Card, Long> {
	@Override
	List<Card> findAll();

	List<Card> findAllByClient(Client client);
}
