package ru.my.opr_vaadin.repository;

import org.springframework.data.repository.CrudRepository;
import ru.my.opr_vaadin.dto.Client;

import java.util.List;

public interface ClientRepository extends CrudRepository<Client, Long> {
	@Override
	List<Client> findAll();
}
