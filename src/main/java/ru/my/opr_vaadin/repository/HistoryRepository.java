package ru.my.opr_vaadin.repository;

import org.springframework.data.repository.CrudRepository;
import ru.my.opr_vaadin.dto.History;

import java.util.List;

public interface HistoryRepository extends CrudRepository<History, Long> {
	@Override
	List<History> findAll();
}
