package ru.my.opr_vaadin.repository;

import org.springframework.data.repository.CrudRepository;
import ru.my.opr_vaadin.dto.Card;
import ru.my.opr_vaadin.dto.Transaction;

import java.util.List;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {
	@Override
	List<Transaction> findAll();

	List<Transaction> findAllByCard(Card card);
}
