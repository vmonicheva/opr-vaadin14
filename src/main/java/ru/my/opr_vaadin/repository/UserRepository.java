package ru.my.opr_vaadin.repository;

import org.springframework.data.repository.CrudRepository;
import ru.my.opr_vaadin.dto.User;

public interface UserRepository extends CrudRepository<User, Long> {

	@Override
	void deleteById(Long aLong);

	@Override
	void delete(User user);

	@Override
	void deleteAll(Iterable<? extends User> iterable);

	@Override
	void deleteAll();

	User findByUsername(String username);
}
