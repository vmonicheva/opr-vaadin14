package ru.my.opr_vaadin.ui.view.admin;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import org.apache.commons.io.IOUtils;
import ru.my.opr_vaadin.dto.Alert;
import ru.my.opr_vaadin.dto.Card;
import ru.my.opr_vaadin.dto.Client;
import ru.my.opr_vaadin.dto.Transaction;
import ru.my.opr_vaadin.repository.AlertRepository;
import ru.my.opr_vaadin.repository.ClientRepository;
import ru.my.opr_vaadin.repository.HistoryRepository;
import ru.my.opr_vaadin.repository.TransactionRepository;
import ru.my.opr_vaadin.ui.view.component.EntityCard;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

@Route(value = "alert/info", layout = MainLayout.class)
@CssImport(value = "./styles/opr-card-form.css", themeFor = "vaadin-form-item")
public class AlertInfoView extends CommonView implements HasUrlParameter<Long> {
	private final AlertRepository alertRepository;
	private final ClientRepository clientRepository;
	private final HistoryRepository historyRepository;
	private final TransactionRepository transactionRepository;
	private Alert alert;

	public AlertInfoView(AlertRepository alertRepository,
	                     ClientRepository clientRepository,
	                     HistoryRepository historyRepository,
	                     TransactionRepository transactionRepository) {
		this.alertRepository = alertRepository;
		this.clientRepository = clientRepository;
		this.historyRepository = historyRepository;
		this.transactionRepository = transactionRepository;
		VerticalLayout mainLayout = new VerticalLayout();
		center.add(mainLayout);
	}


	@Override
	public void setParameter(BeforeEvent event, Long parameter) {
		Optional<Alert> optional = alertRepository.findById(parameter);
		optional.ifPresent(it -> alert = it);
		if (alert != null) {
			fillInfo();
		}
	}

	private void fillInfo() {
		center.removeAll();
		VerticalLayout mainLayout = new VerticalLayout();
		mainLayout.setWidthFull();
		mainLayout.setHeightFull();
		mainLayout.getStyle().set("background-color", "#E5E5E5");
		mainLayout.setPadding(false);
		Label label = new Label("Оповещение №" + alert.getCuid());
		label.getStyle().set("margin-left", "30px");
		label.getStyle().set("margin-top", "30px");
		label.setClassName("opr-label");
		mainLayout.add(label);
		center.add(mainLayout);
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.getStyle().set("padding-left", "30px");
		horizontalLayout.getStyle().set("padding-right", "30px");
		horizontalLayout.getStyle().set("padding", "10px");
		horizontalLayout.setWidthFull();
		horizontalLayout.setHeight("fit-content");
		horizontalLayout.getStyle().set("background-color", "#E5E5E5");
		VerticalLayout innerCardLayout = new VerticalLayout();
		innerCardLayout.getStyle().set("background-color", "#FFFFFF");
		innerCardLayout.getStyle().set("flex-grow", "2");
		innerCardLayout.setHeightFull();
		innerCardLayout.setWidthFull();
		horizontalLayout.add(innerCardLayout);

		VerticalLayout innerButtonLayout = new VerticalLayout();
		innerButtonLayout.setWidth("fit-content");
		innerButtonLayout.setHeight("fit-content");
		Label buttonsLabel = new Label("Применить ручные действия");
		buttonsLabel.setClassName("opr-label");
		buttonsLabel.getStyle().set("font-size", "16px");
		innerButtonLayout.add(buttonsLabel);
		Button toWhite = new Button("Добавить в белый список");
		toWhite.setClassName("opr-action-button");
		innerButtonLayout.add(toWhite);
		Button toBlack = new Button("Добавить в черный список");
		toBlack.setClassName("opr-action-button");
		innerButtonLayout.add(toBlack);
		Button banCredit = new Button("Запретить расходные операции");
		banCredit.setClassName("opr-action-button");
		innerButtonLayout.add(banCredit);
		Button allowCredit = new Button("Разрешить расходные операции");
		allowCredit.setClassName("opr-action-button");
		innerButtonLayout.add(allowCredit);
		Button banCash = new Button("Запретить снятие наличных");
		banCash.setClassName("opr-action-button");
		innerButtonLayout.add(banCash);
		Button allowCash = new Button("Разрешить снятие наличных");
		allowCash.setClassName("opr-action-button");
		innerButtonLayout.add(allowCash);
		Button blockCard = new Button("Заблокировать карту");
		blockCard.setClassName("opr-action-button");
		innerButtonLayout.add(blockCard);
		Button blockAllCards = new Button("Заблокировать все карты");
		blockAllCards.setClassName("opr-action-button");
		innerButtonLayout.add(blockAllCards);
		Button sendReport = new Button("Отправить отчет");
		sendReport.setClassName("opr-action-button");
		innerButtonLayout.add(sendReport);
		innerButtonLayout.getStyle().set("background-color", "#FFFFFF");
		horizontalLayout.add(innerButtonLayout);
		EntityCard clientCard = new EntityCard();
		createClientCard(clientCard);

		mainLayout.add(horizontalLayout);

		EntityCard alertCard = new EntityCard();
		createAlertCard(alertCard);

		EntityCard transactionCard = new EntityCard();
		createTransactionCard(transactionCard);
		innerCardLayout.add(clientCard, alertCard, transactionCard);
	}

	private void createTransactionCard(EntityCard transactionCard) {
		Transaction transaction = alert.getTransaction();
		VerticalLayout main = new VerticalLayout();
		FormLayout layout = new FormLayout();
		layout.setResponsiveSteps(
				new FormLayout.ResponsiveStep("300px", 1, FormLayout.ResponsiveStep.LabelsPosition.TOP),
				new FormLayout.ResponsiveStep("600px", 1, FormLayout.ResponsiveStep.LabelsPosition.ASIDE));
		transactionCard.setSummaryText("Карточная транзакция");

		if (transaction == null) {
			return;
		}
		transactionCard.setSummaryText("Карточная транзакция, " + transaction.getAmount() + " руб., " + transaction.getStatus());
		layout.addFormItem(new Label(transaction.getNumber()), "Номер транзакции").setClassName("opr-form-item-with-border");
		layout.addFormItem(new Label(transaction.getChannel()), "Канал").setClassName("opr-form-item-with-border");
		layout.addFormItem(new Label(transaction.getStatus()), "Статус транзакции").setClassName("opr-form-item-with-border");
		Card card = transaction.getCard();
		layout.addFormItem(new Label(card != null ? card.getNumber() : ""), "Номер карты").setClassName("opr-form-item-with-border");
		layout.addFormItem(new Label(transaction.getAmount().toString()), "Сумма операции").setClassName("opr-form-item-with-border");
		layout.addFormItem(new Label("Рубли"), "Валюта операции");
		main.add(layout);
		Label label = new Label("Последние транзакции");
		label.setClassName("opr-label");
		main.add(label);
		Grid<Transaction> grid = new Grid<>();
		grid.setHeightByRows(true);
		List<Transaction> transactionList = transactionRepository.findAllByCard(transaction.getCard());
		grid.setItems(transactionList);
		grid.addColumn(Transaction::getNumber, "Транзакция").setHeader("Транзакция");

		grid.addColumn((new ComponentRenderer<>((trs) -> {
			Card crd = trs.getCard();
			Image cardImg = getCardImg(crd);
			cardImg.setMaxWidth("50px");
			return new HorizontalLayout(new Label(crd.getNumber()), cardImg);
		})), "Номер карты").setHeader("Номер карты");

		grid.addColumn(Transaction::getAmount, "Сумма").setHeader("Сумма");
		grid.addColumn((ValueProvider<Transaction, String>) trans -> "Рубли", "Валюта").setHeader("Валюта");
		grid.addColumn(Transaction::getStatus, "Статус").setHeader("Статус");
		main.add(grid);
		transactionCard.addContent(main);
	}

	private void createAlertCard(EntityCard alertCard) {
		alertCard.setSummaryText("Алерт: " + alert.getCuid() + ", " + alert.getRule() + ", " + alert.getScoring() + ", " + alert.getStatus());
		VerticalLayout main = new VerticalLayout();
		FormLayout layout = new FormLayout();
		layout.setResponsiveSteps(new FormLayout.ResponsiveStep("300px", 1, FormLayout.ResponsiveStep.LabelsPosition.TOP),
				new FormLayout.ResponsiveStep("600px", 1, FormLayout.ResponsiveStep.LabelsPosition.ASIDE));
		layout.addFormItem(new Label(alert.getCuid().toString()), "Уникальный идентификатор").setClassName("opr-form-item-with-border");
		layout.addFormItem(new Label(alert.getStatus()), "Статус алерта").setClassName("opr-form-item-with-border");
		layout.addFormItem(new Label(alert.getRule()), "Группа правил").setClassName("opr-form-item-with-border");
		layout.addFormItem(new Label(alert.getScoring().toString()), "Скоринг");
		main.add(layout);
		Label label = new Label("История алертов");
		label.setClassName("opr-label");
		main.add(label);
		Grid<Alert> alertHistory = new Grid<>();
		Transaction transaction = alert.getTransaction();
		if (transaction == null) {
			return;
		}
		Card card = transaction.getCard();
		if (card == null) {
			return;
		}
		List<Transaction> transactionList = transactionRepository.findAllByCard(card);
		List<Alert> alertList = alertRepository.findAllByTransactionIn(transactionList);
		alertHistory.setItems(alertList);
		alertHistory.addColumn(Alert::getCuid, "Алерт").setHeader("Алерт");
		alertHistory.addColumn((ValueProvider<Alert, String>) alert -> alert.getTransaction().getStatus(), "Статус транзакции").setHeader("Статус транзакции");
		alertHistory.addColumn((ValueProvider<Alert, String>) alert -> alert.getTransaction().getNumber(), "Транзакция").setHeader("Транзакция");
		alertHistory.addColumn(Alert::getRule, "Правило").setHeader("Правило");
		alertHistory.addColumn(Alert::getScoring, "Скоринг").setHeader("Скоринг");
		alertHistory.addColumn(Alert::getStatus, "Статус алерта").setHeader("Статус алерта");
		main.add(alertHistory);
		alertCard.addContent(main);
	}

	private void createClientCard(EntityCard clientCard) {
		Transaction transaction = alert.getTransaction();
		FormLayout layout = new FormLayout();
		layout.setResponsiveSteps(new FormLayout.ResponsiveStep("300px", 1, FormLayout.ResponsiveStep.LabelsPosition.TOP),
				new FormLayout.ResponsiveStep("600px", 1, FormLayout.ResponsiveStep.LabelsPosition.ASIDE));
		clientCard.setSummaryText("Клиент:");
		clientCard.addContent(layout);
		if (transaction == null) {
			return;
		}
		Client client = transaction.getClient();
		if (client == null) {
			return;
		}
		String name = client.getName();
		clientCard.setSummaryText("Клиент: " + name);
		layout.addFormItem(new Label(name), "ФИО").setClassName("opr-form-item-with-border");
		layout.addFormItem(new Label(client.getCuid().toString()), "CUID").setClassName("opr-form-item-with-border");
		Label phoneLabel = new Label(client.getPhone());
		Button phoneButton = new Button(new Icon(VaadinIcon.PHONE));
		phoneButton.setClassName("opr-phone-button");
		HorizontalLayout phoneLayout = new HorizontalLayout(phoneLabel, phoneButton);
		phoneLayout.setAlignItems(Alignment.CENTER);
		layout.addFormItem(phoneLayout, "Телефон мобильный");
	}

	private Image getCardImg(Card card) {
		String number = card.getNumber();
		InputStream resource;
		if (number.startsWith("4")) {
			resource = getClass().getResourceAsStream("/META-INF/resources/img/visa.png");
		} else if (number.startsWith("9") || number.startsWith("7")) {
			resource = getClass().getResourceAsStream("/META-INF/resources/img/mastercard.png");

		} else {
			resource = getClass().getResourceAsStream("/META-INF/resources/img/mir.png");
		}

		try {
			byte[] bytes = IOUtils.toByteArray(resource);
			StreamResource streamResource = new StreamResource("image.jpg", () -> new ByteArrayInputStream(bytes));
			return new Image(streamResource, "image");
		} catch (IOException e) {
			return null;
		}
	}

}
