package ru.my.opr_vaadin.ui.view.admin;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.dnd.DragSource;
import com.vaadin.flow.component.dnd.DropEffect;
import com.vaadin.flow.component.dnd.DropTarget;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServletRequest;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import me.xdrop.fuzzywuzzy.model.BoundExtractedResult;
import ru.my.opr_vaadin.dto.Alert;
import ru.my.opr_vaadin.dto.Client;
import ru.my.opr_vaadin.dto.Transaction;
import ru.my.opr_vaadin.repository.AlertRepository;
import ru.my.opr_vaadin.ui.view.component.OprPaginatedGrid;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Route(value = "alert", layout = MainLayout.class)
@CssImport(value = "./styles/alert-grid.css", themeFor = "vaadin-grid")
@CssImport(value = "./styles/opr.css")
public class AlertView extends CommonView {
	private final AlertRepository alertRepository;
	private Map<String, String> columnMap = new HashMap<>();
	private OprPaginatedGrid<Alert> alertGrid;
	private Set<Alert> selected = new HashSet<>();


	public AlertView(AlertRepository alertRepository) {
		this.alertRepository = alertRepository;
		init();
	}

	public void init() {
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.getStyle().set("background-color", "#E5E5E5");
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setWidthFull();
		List<Alert> alertList = alertRepository.findAll();
		alertGrid = new OprPaginatedGrid<>();
		HorizontalLayout paginationLayout = alertGrid.getPaginationLayout();
		HorizontalLayout buttons = new HorizontalLayout();
		buttons.setWidthFull();
		Button take = new Button("Взять в работу");
		take.addClickListener(event -> {
			Set<Alert> selectedItems = alertGrid.getSelectedItems();
			if (selectedItems.isEmpty()) {
				return;
			}
			if (selectedItems.size() == 1) {
				take.getUI().ifPresent(ui -> {
					openNewWindow(selectedItems.iterator().next(), ui);
				});
			} else {
				Dialog dialog = new Dialog();
				VerticalLayout layout = new VerticalLayout();
				layout.add(new Label("Взять в работу и открыть в новых вкладках алерты №№" +
				                     selectedItems.stream()
						                     .map(it -> String.valueOf(it.getId()))
						                     .collect(Collectors.joining(",")) + "?"));
				Button open = new Button("Открыть");
				open.addClickListener(e -> {
					take.getUI().ifPresent(ui -> {
						selectedItems.forEach(it ->
								openNewWindow(it, ui));
					});
					dialog.close();
				});
				Button cancel = new Button("Отмена");
				cancel.addClickListener(e -> dialog.close());
				HorizontalLayout horizontalLayout1 = new HorizontalLayout(open, cancel);
				horizontalLayout1.setWidthFull();

				layout.add(horizontalLayout1);
				dialog.add(layout);
				dialog.open();
			}
		});
		take.setThemeName("primary");
		buttons.add(take);
		Button resolution = new Button("Указать резолюцию");
		resolution.setThemeName("primary");
		buttons.add(resolution);
		buttons.add(new Button("Вернуть в очередь"));
		buttons.add(new Button("Назначить исполнителя"));
		buttons.add(new Button("Эскалировать"));
		paginationLayout.addComponentAtIndex(0, buttons);
		alertGrid.setHeightByRows(false);
		alertGrid.setPaginatorTexts("Страница", "из");
		alertGrid.setItems(alertList);
		alertGrid.setSelectionMode(Grid.SelectionMode.MULTI);
		alertGrid.setColumnReorderingAllowed(true);
		alertGrid.addSelectionListener(e -> {
			selected.addAll(e.getAllSelectedItems());
		});
		alertGrid.addPageChangeListener(event -> {
			selected.forEach(it -> alertGrid.select(it));
		});
		alertGrid.addColumn(new ComponentRenderer<>((alert) -> {
			Icon icon = new Icon(VaadinIcon.SEARCH);
			icon.addClickListener(e -> {
				icon.getUI().ifPresent(ui -> ui.navigate(AlertInfoView.class, alert.getId()));
			});
			icon.getStyle().set("cursor", "pointer");
			icon.setSize("15px");
			return icon;
		}))
				.setFlexGrow(0)
				.setWidth("50px");

		Grid.Column<Alert> idColumn = alertGrid.addColumn(Alert::getId, "ID")
				.setHeader("ID")
				.setFlexGrow(0)
				.setWidth("100px")
				.setKey("id");
		columnMap.put(idColumn.getKey(), "ID");

		Grid.Column<Alert> cuidColumn = alertGrid.addColumn(Alert::getCuid, "CUID")
				.setHeader("CUID")
				.setFlexGrow(0)
				.setWidth("150px")
				.setKey("cuid");
		columnMap.put(cuidColumn.getKey(), "CUID");

		Grid.Column<Alert> clientColumn = alertGrid.addColumn((ValueProvider<Alert, String>) alert -> {
			Transaction transaction = alert.getTransaction();
			if (transaction == null) {
				return "";
			}
			Client client = transaction.getClient();
			return client != null ? client.getName() : "";
		}, "Клиент")
				.setHeader("Клиент")
				.setKey("client");
		columnMap.put(clientColumn.getKey(), "Клиент");

		Grid.Column<Alert> transactionStatusColumn = alertGrid.addColumn((ValueProvider<Alert, String>) alert -> {
			Transaction transaction = alert.getTransaction();
			return transaction != null ? transaction.getStatus() : "";
		}, "Статус транзакции")
				.setHeader("Статус транзакции")
				.setKey("transactionStatus");
		columnMap.put(transactionStatusColumn.getKey(), "Статус транзакции");

		Grid.Column<Alert> transactionColumn = alertGrid.addColumn((ValueProvider<Alert, String>) alert -> {
			Transaction transaction = alert.getTransaction();
			return transaction != null ? transaction.getNumber() : "";
		}, "Транзакция")
				.setHeader("Транзакция")
				.setKey("transaction");
		columnMap.put(transactionColumn.getKey(), "Транзакция");

		Grid.Column<Alert> ruleColumn = alertGrid.addColumn(Alert::getRule, "Правило")
				.setHeader("Правило")
				.setFlexGrow(0)
				.setWidth("150px")
				.setKey("rule");
		columnMap.put(ruleColumn.getKey(), "Правило");

		Grid.Column<Alert> scoringColumn = alertGrid.addColumn(Alert::getScoring, "Скоринг")
				.setHeader("Скоринг")
				.setKey("scoring");
		columnMap.put(scoringColumn.getKey(), "Скоринг");

		Grid.Column<Alert> statusColumn = alertGrid.addColumn(Alert::getStatus, "Статус")
				.setHeader("Статус")
				.setKey("status");
		columnMap.put(statusColumn.getKey(), "Статус");

		Icon headerComponent = new Icon(VaadinIcon.COG_O);
		List<Grid.Column<Alert>> columns = Arrays.asList(idColumn, cuidColumn, clientColumn, transactionStatusColumn, transactionColumn, ruleColumn, scoringColumn, statusColumn);
		ContextMenu contextMenu = new ContextMenu();
		contextMenu.setOpenOnClick(true);
		contextMenu.setTarget(headerComponent);
		columns.forEach(alertColumn -> {
			String key = alertColumn.getKey();
			Checkbox checkbox = new Checkbox(columnMap.get(key));
			checkbox.setValue(alertGrid.getColumnByKey(key).isVisible());
			checkbox.addClickListener(click -> {
				alertGrid.getColumnByKey(key).setVisible(checkbox.getValue());
			});
			contextMenu.addItem(checkbox);
		});
		headerComponent.setSize("20px");
		headerComponent.getStyle().set("cursor", "pointer");
		alertGrid.addColumn((ValueProvider<Alert, String>) alert -> "").setHeader(headerComponent).setFlexGrow(0).setWidth("60px");
		alertGrid.setClassNameGenerator(alert -> {
			if ("Черное".equals(alert.getRule())) {
				return "opr-row-blackrule";
			} else if ("Эскалировано".equals(alert.getStatus())) {
				return "opr-row-escalated";
			} else {
				return null;
			}
		});

		TextField search = new TextField();
		search.setPlaceholder("Поиск");
		search.setWidthFull();
		horizontalLayout.add(search);
		Button button = new Button("Найти");
		button.addClickListener(e -> {
			String value = search.getValue();
			if (value == null || "".equals(value)) {
				alertGrid.setItems(alertList);
			} else {
				List<BoundExtractedResult<Alert>> boundExtractedResults = FuzzySearch.extractTop(value, alertList, Alert::toString, 100, 60);
				List<Alert> result = boundExtractedResults.stream().map(it -> it.getReferent()).collect(Collectors.toList());
				alertGrid.setItems(result);
			}
			alertGrid.getDataProvider().refreshAll();
		});
		button.getElement().setAttribute("theme", "primary");
		horizontalLayout.add(button);
		VerticalLayout innerLayout = new VerticalLayout();
		innerLayout.setHeightFull();
		innerLayout.getStyle().set("background-color", "#FFFFFF");
		innerLayout.add(horizontalLayout);
		innerLayout.add(alertGrid);
		Label label = new Label("Оповещения");
		label.setClassName("opr-label");
		verticalLayout.add(label);
		verticalLayout.add(innerLayout);
		alertGrid.setPageSize(15);

		// Sets how many pages should be visible on the pagination before and/or after the current selected page
		alertGrid.setPaginatorSize(2);

		center.add(verticalLayout);
	}

	private void openNewWindow(Alert selectedItem, UI ui) {
		RouteConfiguration configuration = RouteConfiguration
				.forRegistry(ui.getRouter().getRegistry());
		String url = configuration.getUrl(AlertInfoView.class, selectedItem.getId());
		VaadinServletRequest currentRequest = (VaadinServletRequest) VaadinService.getCurrentRequest();
		String serverName = currentRequest.getServerName();
		int serverPort = currentRequest.getServerPort();
		String port = serverPort == 80 ? "" : ":" + serverPort;
		UI.getCurrent().getPage().open("http://" + serverName + port + "/" + url);
	}

	private void showSettingsDialog(List<Grid.Column<Alert>> columns) {
		Dialog dialog = new Dialog();
		VerticalLayout verticalLayout = getVerticalLayout(columns);
		dialog.add(verticalLayout);
		dialog.open();
	}

	private VerticalLayout getVerticalLayout(List<Grid.Column<Alert>> columns) {
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setHeightFull();
		HorizontalLayout header = new HorizontalLayout();
		Label label = new Label("Ячейки таблицы");
		Icon closeIcon = new Icon(VaadinIcon.CLOSE);
		closeIcon.setSize("15px");
		closeIcon.getStyle().set("cursor", "pointer");
//		closeIcon.addClickListener(e -> {
//			dialog.close();
//		});
		header.setAlignItems(Alignment.CENTER);
		header.setJustifyContentMode(JustifyContentMode.BETWEEN);
		header.add(label, closeIcon);
		verticalLayout.add(header);
		DropTarget<VerticalLayout> dropTarget = DropTarget.create(verticalLayout);
		dropTarget.addDropListener(event -> {
			// move the dragged component to inside the drop target component
			if (event.getDropEffect() == DropEffect.MOVE) {
				// the drag source is available only if the dragged component is from
				// the same UI as the drop target
				event.getDragSourceComponent().ifPresent(verticalLayout::add);

				event.getDragData().ifPresent(data -> {
					// the server side drag data is available if it has been set and the
					// component was dragged from the same UI as the drop target
				});
			}
		});

		columns.forEach(alertColumn -> {
			String key = alertColumn.getKey();
			Checkbox checkbox = new Checkbox(columnMap.get(key));
			checkbox.setValue(alertGrid.getColumnByKey(key).isVisible());
			checkbox.addClickListener(e -> {
				alertGrid.getColumnByKey(key).setVisible(checkbox.getValue());
			});
			HorizontalLayout row = new HorizontalLayout(checkbox);
			DragSource<HorizontalLayout> box1DragSource = DragSource.create(row);
			box1DragSource.setDraggable(true);
			verticalLayout.add(row);
		});
		return verticalLayout;
	}
}
