package ru.my.opr_vaadin.ui.view.admin;

import com.vaadin.flow.component.orderedlayout.FlexLayout;

public class CommonView extends FlexLayout {
	public static final String PAGE_TITLE = "Администратор";
	protected final FlexLayout center = new FlexLayout();

	public CommonView() {
		add(center);
		center.setWidthFull();
		setSizeFull();
	}
}