package ru.my.opr_vaadin.ui.view.admin;

import org.springframework.stereotype.Service;
import ru.my.opr_vaadin.dto.Alert;
import ru.my.opr_vaadin.dto.Card;
import ru.my.opr_vaadin.dto.Client;
import ru.my.opr_vaadin.dto.History;
import ru.my.opr_vaadin.dto.Transaction;
import ru.my.opr_vaadin.repository.AlertRepository;
import ru.my.opr_vaadin.repository.CardRepository;
import ru.my.opr_vaadin.repository.ClientRepository;
import ru.my.opr_vaadin.repository.HistoryRepository;
import ru.my.opr_vaadin.repository.TransactionRepository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class Filler {
	private final AlertRepository alertRepository;
	private final ClientRepository clientRepository;
	private final HistoryRepository historyRepository;
	private final TransactionRepository transactionRepository;
	private final CardRepository cardRepository;
	private final List<String> operators = Arrays.asList("Одинцов В.В.", "Субботин Х.М.", "Гамула М.М.", "Волкова Б.А.");
	private final List<String> transactionStatuses = Arrays.asList("В очереди", "Операция отклонена", "Операция приостановлена");
	private final List<String> statuses = Arrays.asList("В очереди", "В работе", "Эскалировано");
	private final List<String> rules = Arrays.asList("Серое", "Черное");

	public Filler(AlertRepository alertRepository,
	              ClientRepository clientRepository,
	              HistoryRepository historyRepository,
	              TransactionRepository transactionRepository,
	              CardRepository cardRepository) {
		this.alertRepository = alertRepository;
		this.clientRepository = clientRepository;
		this.historyRepository = historyRepository;
		this.transactionRepository = transactionRepository;
		this.cardRepository = cardRepository;
	}

	public void load() {
		List<Client> clients = createClients();
		List<Transaction> transactions = createTransactions(clients);
		List<Alert> alerts = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			Alert alert = new Alert();
			alert.setCuid((long) (1111 + (int) (Math.random() * 11111)));
			alert.setStatus(statuses.get((int) (Math.random() * 3)));
			alert.setTransaction(transactions.get((int) (Math.random() * 3)));
			alert.setRule(rules.get((int) (Math.random() * 2)));
			alert.setScoring((10 + (int) (Math.random() * 1000)));
			alerts.add(alert);
		}
		alertRepository.saveAll(alerts);
		createHistory(alerts);
	}

	private void createHistory(List<Alert> alerts) {
		List<History> histories = new ArrayList<>();
		alerts.forEach(alert -> {
			History create = new History();
			create.setAction("Алерт создан");
			create.setAlert(alert);
			create.setAuthor("Система");
			Timestamp createTime = Timestamp.valueOf(LocalDateTime.now().minusHours(((int) (Math.random() * 5))));
			create.setDateTime(createTime);
			histories.add(create);
			if (statuses.get(1).equals(alert.getStatus())) {
				histories.add(createInWork(alert, createTime));
			} else if (statuses.get(2).equals(alert.getStatus())) {
				histories.add(createInWork(alert, createTime));
				histories.add(createEscalated(alert));
			}
		});
		historyRepository.saveAll(histories);
	}

	private History createEscalated(Alert alert) {
		History history = new History();
		history.setAction("Эскалирован");
		history.setAlert(alert);
		LocalDateTime now = LocalDateTime.now();
		history.setDateTime(Timestamp.valueOf(now));
		history.setAuthor(operators.get((int) (Math.random() * 4)));
		return history;
	}

	private History createInWork(Alert alert, Timestamp createTime) {
		History history = new History();
		history.setAction("Взят в работу");
		history.setAlert(alert);
		history.setDateTime(Timestamp.valueOf(createTime.toLocalDateTime().plusHours((int) (Math.random() * 5))));
		history.setAuthor(operators.get((int) (Math.random() * 4)));
		return history;
	}

	private List<Transaction> createTransactions(List<Client> clients) {
		List<Transaction> result = new ArrayList<>();
		for (int i = 0; i < 50; i++) {
			Transaction transaction = new Transaction();
			transaction.setNumber(String.valueOf(1111 + (int) (Math.random() * 11111)));
			transaction.setAmount(BigDecimal.valueOf((1000 + (int) (Math.random() * 11111))));
			Client client = clients.get((int) (Math.random() * clients.size()));
			transaction.setClient(client);
			List<Card> cardList = cardRepository.findAllByClient(client);
			transaction.setCard(cardList.get((int) (Math.random() * cardList.size())));
			transaction.setChannel("ДБО");
			transaction.setStatus(transactionStatuses.get((int) (Math.random() * 2)));
			result.add(transaction);
		}
		transactionRepository.saveAll(result);
		return result;
	}

	private List<Client> createClients() {
		List<Client> result = new ArrayList<>();
		Client client1 = new Client();
		client1.setCuid((long) (1111 + (int) (Math.random() * 11111)));
		client1.setName("Бачей О.Е.");
		client1.setPhone("8(983)696-56-21");
		client1.setSex("Мужской");
		result.add(client1);

		Client client2 = new Client();
		client2.setCuid((long) (1111 + (int) (Math.random() * 11111)));
		client2.setName("Иванов С.И.");
		client2.setPhone("7(599)902-93-92");
		client2.setSex("Мужской");
		result.add(client2);

		Client client3 = new Client();
		client3.setCuid((long) (1111 + (int) (Math.random() * 11111)));
		client3.setName("Трублаевский И.Р.");
		client3.setPhone("7(997)955-41-33");
		client3.setSex("Мужской");
		result.add(client3);

		Client client4 = new Client();
		client4.setCuid((long) (1111 + (int) (Math.random() * 11111)));
		client4.setName("Андрейко К.Е.");
		client4.setPhone("8(957)434-51-98");
		client4.setSex("Мужской");
		result.add(client4);
		clientRepository.saveAll(result);

		Card card1 = new Card("4231 9276 6201 6405", client1);
		Card card2 = new Card("5455 5273 5959 4362", client2);
		Card card3 = new Card("9581 1712 4876 8967", client3);
		Card card4 = new Card("4373 4419 4121 1126", client4);
		Card card5 = new Card("7780 0960 0740 1801", client1);
		Card card6 = new Card("9638 5497 6806 4481", client2);
		Card card7 = new Card("7994 7645 6361 1743", client3);
		Card card8 = new Card("4335 9715 6328 3480", client4);
		cardRepository.saveAll(Arrays.asList(card1, card2, card3, card4, card5, card6, card7, card8));
		return result;
	}
}
