package ru.my.opr_vaadin.ui.view.admin;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.RouterLink;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ru.my.opr_vaadin.ui.view.component.NavigationPanel;
import ru.my.opr_vaadin.ui.view.login.MainView;

import java.util.HashMap;
import java.util.Map;

import static ru.my.opr_vaadin.security.Roles.ROLE_ADMIN;
import static ru.my.opr_vaadin.ui.view.admin.CommonView.PAGE_TITLE;

@Secured(ROLE_ADMIN)
@PageTitle(PAGE_TITLE)
public class MainLayout extends AppLayout implements BeforeEnterObserver {
	private final Tabs tabs = new Tabs();
	private final Map<Class<? extends Component>, Tab> navigationTargetToTab = new HashMap<>();

	public MainLayout() {
		setPrimarySection(AppLayout.Section.DRAWER);
		addMenuTab("Оповещения", AlertView.class);
		addMenuTab("Рекламации", ReclamationView.class);
		tabs.setOrientation(Tabs.Orientation.VERTICAL);
		NavigationPanel navigationPanel = new NavigationPanel(tabs);
		addToDrawer(navigationPanel);
	}

	private void addMenuTab(String label, Class<? extends Component>... targets) {
		Tab tab = new Tab(new RouterLink(label, targets[0]));
		for (Class<? extends Component> target : targets) {
			navigationTargetToTab.put(target, tab);
		}
		tabs.add(tab);
	}

	protected void navigate(Class<? extends Component> view) {
		getUI().ifPresent(ui ->
				ui.navigate(view));
	}

	@Override
	public void beforeEnter(BeforeEnterEvent event) {

		tabs.setSelectedTab(navigationTargetToTab.get(event.getNavigationTarget()));
	}

	protected void logout() {
		//https://stackoverflow.com/a/5727444/1572286
		SecurityContextHolder.clearContext();
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		if (requestAttributes instanceof ServletRequestAttributes) {
			((ServletRequestAttributes) requestAttributes).getRequest().getSession().invalidate();
		}

		getUI().ifPresent(ui -> ui.getPage().setLocation(MainView.ROUTE));
	}
}