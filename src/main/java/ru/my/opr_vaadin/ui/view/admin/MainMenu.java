package ru.my.opr_vaadin.ui.view.admin;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.security.access.annotation.Secured;

import static ru.my.opr_vaadin.security.Roles.ROLE_ADMIN;
import static ru.my.opr_vaadin.ui.view.admin.CommonView.PAGE_TITLE;

@Secured(ROLE_ADMIN)
@Route(value = "admin", layout = MainLayout.class)
@PageTitle(PAGE_TITLE)
public class MainMenu extends CommonView {
	private final Filler filler;

	public MainMenu(Filler filler) {
		this.filler = filler;
		VerticalLayout mainLayout = new VerticalLayout();
		center.add(mainLayout);
		center.getStyle().set("background", "unset");
		center.getStyle().set("background-size", "cover");
		Button load = new Button("Загрузить тестовые данные");
		load.addClickListener(e -> {
			filler.load();
		});
		mainLayout.add(load);
	}
}
