package ru.my.opr_vaadin.ui.view.component;

import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.details.DetailsVariant;
@CssImport(value = "./styles/opr-entity-card.css", themeFor = "vaadin-details")
public class EntityCard extends Details implements HasStyle {
	public EntityCard() {
		addThemeVariants(DetailsVariant.REVERSE);
		setClassName("opr-card-entity");
	}
}
