package ru.my.opr_vaadin.ui.view.component;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tabs;

public class NavigationPanel extends VerticalLayout {
	private Tabs tabs;

	public NavigationPanel(Tabs tabs) {
		this.tabs = tabs;
		setPadding(false);
		init();
	}

	private void init() {
		setHeightFull();
		VerticalLayout verticalLayout = new VerticalLayout(tabs);
		verticalLayout.setHeightFull();
		add(verticalLayout);
		Icon icon = new Icon(VaadinIcon.SIGN_OUT_ALT);
		icon.getStyle().set("transform", "rotate(180deg)");
		Button collapseButton = new Button("Свернуть панель", icon);
		collapseButton.getStyle().set("background-color", "unset");
		collapseButton.getStyle().set("cursor", "pointer");
		collapseButton.setWidthFull();
		collapseButton.addClickListener(e -> {
//			fireEvent(new CollapseEvent<VerticalLayout>(this, true, this));
		});
		HorizontalLayout buttonLayout = new HorizontalLayout(collapseButton);
		buttonLayout.setWidthFull();
		add(buttonLayout);
	}
}
