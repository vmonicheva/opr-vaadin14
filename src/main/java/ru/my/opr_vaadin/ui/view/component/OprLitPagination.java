package ru.my.opr_vaadin.ui.view.component;

import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.shared.Registration;
import org.vaadin.klaudeta.LitPagination;

public class OprLitPagination extends LitPagination {
	@Override
	public Registration addPageChangeListener(ComponentEventListener<PageChangeEvent> listener) {
		return super.addPageChangeListener(listener);
	}
}
