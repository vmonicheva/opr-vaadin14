package ru.my.opr_vaadin.ui.view.component;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.provider.QuerySortOrder;
import com.vaadin.flow.function.SerializableComparator;
import com.vaadin.flow.shared.Registration;
import org.vaadin.klaudeta.LitPagination;
import org.vaadin.klaudeta.PaginatedGrid;

import java.util.List;
import java.util.Objects;

public class OprPaginatedGrid<T> extends Grid<T> {
	private final OprLitPagination pagination;

	private PaginatedGrid.PaginationLocation paginationLocation = PaginatedGrid.PaginationLocation.BOTTOM;

	private DataProvider<T, ?> dataProvider;
	private HorizontalLayout paginationLayout;

	public OprPaginatedGrid() {
		pagination = new OprLitPagination();
		this.dataProvider = super.getDataProvider();
		this.setHeightByRows(true);
		paginationLayout = new HorizontalLayout();
		paginationLayout.setWidthFull();
		pagination.addPageChangeListener(e -> doCalcs(e.getNewPage()));
		addSortListener(e -> doCalcs(pagination.getPage()));
	}

	@Override
	protected void onAttach(AttachEvent attachEvent) {
		super.onAttach(attachEvent);
		getParent().ifPresent(p -> {
			int indexOfChild = p.getElement().indexOfChild(this.getElement());

			Span wrapper = new Span(pagination);
			wrapper.getElement().getStyle().set("width", "100%");
			wrapper.getElement().getStyle().set("display", "flex");
			wrapper.getElement().getStyle().set("justify-content", "flex-end");
			paginationLayout.add(wrapper);

			//this moves the pagination element below the grid
			if (paginationLocation == PaginatedGrid.PaginationLocation.BOTTOM) {
				indexOfChild++;
			}

			p.getElement().insertChild(indexOfChild, paginationLayout.getElement());
		});

		doCalcs(0);
	}

	public HorizontalLayout getPaginationLayout() {
		return paginationLayout;
	}

	private void doCalcs(int newPage) {
		int offset = newPage > 0 ? (newPage - 1) * this.getPageSize() : 0;

		InnerQuery query = new InnerQuery<>(offset);

		pagination.setTotal(dataProvider.size(query));

		super.setDataProvider(DataProvider.fromStream(dataProvider.fetch(query)));

	}

	public void refreshPaginator() {
		if (pagination != null) {
			pagination.setPageSize(getPageSize());
			pagination.setPage(1);
			if (dataProvider != null) {
				doCalcs(pagination.getPage());
			}
			pagination.refresh();
		}
	}

	@Override
	public void setPageSize(int pageSize) {
		super.setPageSize(pageSize);
		refreshPaginator();

	}

	public int getPage() {
		return pagination.getPage();
	}

	public void setPage(int page) {
		pagination.setPage(page);
	}

	/**
	 * @return the location of the pagination element
	 */
	public PaginatedGrid.PaginationLocation getPaginationLocation() {
		return paginationLocation;
	}

	/**
	 * setter of pagination location
	 * @param paginationLocation either PaginationLocation.TOP or PaginationLocation.BOTTOM
	 */
	public void setPaginationLocation(PaginatedGrid.PaginationLocation paginationLocation) {
		this.paginationLocation = paginationLocation;
	}

	@Override
	public void setHeightByRows(boolean heightByRows) {
		getElement().setProperty("heightByRows", heightByRows);
	}


	/**
	 * Sets the count of the pages displayed before or after the current page.
	 * @param size
	 */
	public void setPaginatorSize(int size) {
		pagination.setPage(1);
		pagination.setPaginatorSize(size);
		pagination.refresh();
	}

	/**
	 * Sets the texts they are displayed on the paginator. This method is useful
	 * when localization of the component is applicable.
	 * @param pageText the text to display for the `Page` term in the Paginator
	 * @param ofText   the text to display for the `of` term in the Paginator
	 */
	public void setPaginatorTexts(String pageText, String ofText) {
		pagination.setPageText(pageText);
		pagination.setOfText(ofText);
	}

	@Override
	public void setDataProvider(DataProvider<T, ?> dataProvider) {
		Objects.requireNonNull(dataProvider, "DataProvider shoul not be null!");

		if (!Objects.equals(this.dataProvider, dataProvider)) {
			this.dataProvider = dataProvider;
			this.dataProvider.addDataProviderListener(event -> {
				refreshPaginator();
			});
			refreshPaginator();
		}

	}

	/**
	 * Adds a ComponentEventListener to be notified with a PageChangeEvent each time
	 * the selected page changes.
	 * @param listener to be added
	 * @return registration to unregister the listener from the component
	 */
	public Registration addPageChangeListener(ComponentEventListener<LitPagination.PageChangeEvent> listener) {
		return pagination.addPageChangeListener(listener);
	}

	/**
	 * Enumeration to define the location of the element relative to the grid
	 **/
	public enum PaginationLocation {TOP, BOTTOM}

	private class InnerQuery<F> extends Query<T, F> {

		InnerQuery() {
			this(0);
		}

		InnerQuery(int offset) {
			super(offset, getPageSize(), getDataCommunicator().getBackEndSorting(), getDataCommunicator().getInMemorySorting(), null);
		}

		InnerQuery(int offset, List<QuerySortOrder> sortOrders, SerializableComparator<T> serializableComparator) {
			super(offset, getPageSize(), sortOrders, serializableComparator, null);
		}

	}
}
