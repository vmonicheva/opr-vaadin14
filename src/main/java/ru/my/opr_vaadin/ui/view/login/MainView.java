package ru.my.opr_vaadin.ui.view.login;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.ReconnectDialogConfiguration;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dependency.NpmPackage;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.dom.Element;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Route(MainView.ROUTE)
@RouteAlias("")
@PageTitle("Логин")
@NpmPackage(value = "@polymer/iron-form", version = "3.0.1")
@JsModule("@polymer/iron-form/iron-form.js")
@Theme(value = Lumo.class)
public class MainView extends FlexLayout implements BeforeEnterObserver {

	public static final String ROUTE = "login";
	private static final String LOGIN_FORM_ID = "ironform";
	private static final String version;

	static {
		try {
			URL resource = MainView.class.getResource("/META-INF/resources/git.json");
			if (resource == null) {
				version = "DEV";
			} else {
				try (InputStream inputStream = resource.openStream()) {
					Map<String, String> kv = new HashMap<>();
					new BufferedReader(new InputStreamReader(inputStream)).lines().forEach(
							it -> {
								String[] keyValue = it.split(" : ");
								if (keyValue.length != 2) {
									return;
								}
								String key = keyValue[0].trim();
								String value = keyValue[1].trim();
								key = key.substring(1, key.length() - 1);
								if (value.endsWith(",")) {
									value = value.substring(1, value.length() - 2);
								} else {
									value = value.substring(1, value.length() - 1);
								}
								kv.put(key, value);
							}
					);
					version = kv.get("git.tags");
				}
			}
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	private final Element loginForm;

	public MainView() {
		ReconnectDialogConfiguration configuration = UI.getCurrent().getReconnectDialogConfiguration();
		configuration.setDialogText("Восстанавливаем соединение с сервером... ожидайте...");

		getStyle().set("flex-direction", "column");

		TextField userNameTextField = new TextField();
		userNameTextField.setId("tf-username");
		userNameTextField.getElement().setAttribute("name", "username");
		PasswordField passwordField = new PasswordField();
		passwordField.setId("tf-password");
		passwordField.getElement().setAttribute("name", "password");

		Button submitButton = new Button("Войти");
		submitButton.setId("submitbutton");
		submitButton.getElement().setAttribute("theme", "dark");
		submitButton.addClickShortcut(Key.ENTER);
		submitButton.addClickListener((e) -> UI.getCurrent().getPage().executeJs("document.getElementById('" + LOGIN_FORM_ID + "').submit();"));

		FlexLayout loginFormLayout = new FlexLayout();
		loginFormLayout.getStyle().set("flex-direction", "column");
		loginFormLayout.add(userNameTextField, passwordField, submitButton);

		Element formElement = new Element("form");
		formElement.setAttribute("method", "post");
		formElement.setAttribute("action", "login");
		formElement.appendChild(loginFormLayout.getElement());

		loginForm = new Element("iron-form");
		loginForm.setAttribute("id", LOGIN_FORM_ID);
		loginForm.setAttribute("allow-redirect", true);
		loginForm.appendChild(formElement);

		FlexLayout innerLayout = new FlexLayout();
		innerLayout.getStyle().set("flex-direction", "column");
		innerLayout.getStyle().set("max-width", "400px");
		innerLayout.getElement().appendChild(loginForm);
		add(innerLayout);

		FlexLayout verticalLayout = new FlexLayout(
				createDevLogin("Администратор", "admin", "admin")
		);
		verticalLayout.getStyle().set("flex-direction", "column");
		verticalLayout.setWidthFull();
		innerLayout.add(verticalLayout);


		Label div = new Label("Версия: " + version);
		div.getStyle().set("position", "absolute");
		div.getStyle().set("left", "10px");
		div.getStyle().set("bottom", "10px");
		add(div);
	}

	private Button createDevLogin(String name, String username, String password) {
		Button submitButton = new Button(name);
		submitButton.getElement().setAttribute("theme", "dark");
		submitButton.setId("submitbutton");
		submitButton.addClickShortcut(Key.ENTER);
		submitButton.addClickListener((e) -> UI.getCurrent().getPage().executeJs(
				"document.getElementById('tf-username').value='" + username + "';" +
				"document.getElementById('tf-password').value='" + password + "';" +
				"document.getElementById('" + LOGIN_FORM_ID + "').submit();")
		);
		return submitButton;
	}


	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		// inform the user about an authentication error
		// (yes, the API for resolving query parameters is annoying...)
		if (!event.getLocation().getQueryParameters().getParameters().getOrDefault("error", Collections.emptyList()).isEmpty()) {
			addErrorMessage();
		}
	}

	private void addErrorMessage() {
		Element title = new Element("div");
		title.setProperty("innerHTML", "Неверное имя пользователя или пароль<br>Проверьте корректность вводимых данных и повторите вход");
		loginForm.insertChild(0, title);
	}
}